#!/bin/sh

set -e

TARGET="$HOME/.config/nixpkgs"

EMAIL_PLACEHOLDER="example@example.com"
EMAIL="$(cat "$HOME/.config/email")"

REPLACE_OP="s/$EMAIL_PLACEHOLDER/$EMAIL/g"

mkdir -p "$TARGET"
cp *.nix "$TARGET"

find "$TARGET" -type f -exec sed -i "$REPLACE_OP" {} \;
